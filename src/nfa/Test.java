/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nfa;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USUARIO
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // TODO code application logic here
            NFA nfa=new NFA("test.txt");
            System.out.println(nfa);
            for(Integer i:nfa.stateMap.values()){
                System.out.println(nfa.epsilonClosure(i));
            }
            System.out.println(nfa.charMap);
            nfa.toDFA();
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
