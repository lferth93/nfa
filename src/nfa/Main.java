/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nfa;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author USUARIO
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        NFA nfa = null;
        Scanner s = new Scanner(System.in);
        int op = 0;
        while (true) {
            try {
                if (op == 0) {
                    String file;
                    System.out.println("Ingresar el nombre del archivo");
                    file = s.next().trim();
                    nfa = new NFA(file);
                    System.out.println(nfa);
                    System.out.println("El archivo " + file + " fue cargado con exito");
                } else if (op == 1) {
                    System.out.println("Ingresar la palabra");
                    String test = s.next();
                    ArrayList<NFA.Path> paths = nfa.acept(test);
                    if (paths==null || paths.isEmpty()) {
                        System.out.println("La palabra " + test + " no fue aceptada");
                    }
                    for (NFA.Path path : paths) {
                        System.out.println(nfa.buildPath(path,test));
                    }
                } else if (op == 2) {
                    break;
                } else {
                    System.out.println("Opcion invalida");
                }
                System.out.println("Elija una opcion");
                System.out.println("[0] Cargar un archivo");
                System.out.println("[1] Probar una palabra");
                System.out.println("[2] Salir");
                op = 3;
                op = Integer.parseInt(s.next());
            } catch (Exception ex) {
                System.out.println("Inente con un archivo diferente");
                ex.printStackTrace();
            }
        }
    }
}
