package nfa;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author USUARIO
 */
public class NFA {

    final char EP = 'E';
    int start;//estado inicial
    HashMap<String, Integer> stateMap;//relaciona los estados con enteros
    String[] stateArray;//relaciona un entero con un estado
    HashMap<Character, Integer> charMap; //relaciona un caracter con un entero
    char[] charArray;//relaciona un entero con un caracter
    ArrayList<Integer>[][] deltha;//tabla de transiciones 
    boolean[] isFinal;//conjunto de estados finales

    //clase para guardar un recorrido
    public class Path extends ArrayList<Pair<Integer, Integer>> {

        public Path(int start) {
            super();
            this.add(new Pair(0, start));
        }

        public int state() {
            return this.get(this.size() - 1).getSecond();
        }
    }

    //construye el objeto desde un archivo de texto
    public NFA(String file) throws Exception {
        try {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            stateMap = new HashMap<>();
            charMap = new HashMap<>();
            this.stateArray = br.readLine().split(",");//guarda el conjunto de estados permite estados como cadenas
            for (int i = 0; i < stateArray.length; i++) {
                stateArray[i] = stateArray[i].trim();
                stateMap.put(stateArray[i], i);
            }
            charMap.put(EP, 0);//guarda el caracter epsilon como primer caracter
            String[] alfabeto = br.readLine().split(",");//guarda un arreglo con el alfabeto en cadenas
            this.charArray = new char[alfabeto.length + 1];//arreglo para guardar el alfabeto en caracteres
            charArray[0] = EP;
            for (int i = 0; i < alfabeto.length; i++) {//guarda el alfabeto en el arreglo y el mapa
                charArray[i + 1] = alfabeto[i].charAt(0);
                charMap.put(charArray[i + 1], i + 1);
            }
            isFinal = new boolean[stateArray.length];
            start = stateMap.get(br.readLine().trim());
            String[] fin = br.readLine().split(",");
            for (int i = 0; i < fin.length; i++) {//guarda los estados finales
                fin[i] = fin[i].trim();
                isFinal[stateMap.get(fin[i])] = true;
            }
            String line;
            deltha = new ArrayList[stateArray.length][charArray.length];
            while ((line = br.readLine()) != null) {//lee las lineas de la tabla de transiciones y las guarda en deltha
                String[] del = line.split(",");
                int s = stateMap.get(del[0]);
                int c = charMap.get(del[1].charAt(0));
                if (deltha[s][c] == null) {
                    deltha[s][c] = new ArrayList();
                }
                deltha[s][c].add(stateMap.get(del[2]));
            }
            br.close();
            fr.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado");
            throw new Exception();
        } catch (IOException | NullPointerException ex) {
            System.out.println("Archivo con formato incorrecto");
            throw new Exception();
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Q ");
        sb.append(Arrays.toString(stateArray)).append('\n');
        sb.append("\u03A3 ");
        sb.append(Arrays.toString(charArray)).append('\n');
        sb.append("S ").append(stateArray[start]).append('\n');
        sb.append("F ");
        ArrayList<String> f = new ArrayList();
        for (int i = 0; i < isFinal.length; i++) {
            if (isFinal[i]) {
                f.add(stateArray[i]);
            }
        }
        sb.append(f).append('\n');
        sb.append("\u03B4\n");
        for (int i = 0; i < deltha.length; i++) {
            for (int j = 0; j < deltha[i].length; j++) {
                if (deltha[i][j] == null || deltha[i][j].isEmpty()) {
                    if (j > 0) {
                        sb.append(stateArray[i]).append(", ").append(charArray[j]).append(", err").append('\n');
                    }
                } else {
                    for (Integer s : deltha[i][j]) {
                        sb.append(stateArray[i]).append(", ").append(charArray[j]).append(", ").append(stateArray[s]).append('\n');
                    }
                }
            }
        }
        return sb.toString();
    }

    public ArrayList<Path> acept(String str) {
        ArrayList<Path> ret = new ArrayList();//lista de recorridos aceptados
        ArrayList<Path> activos = new ArrayList();
        //activos[start] = new ArrayList();
        activos.add(new Path(start));
        for (int i = 0; i < str.length(); i++) {
            if (!charMap.containsKey(str.charAt(i))) {
                //System.out.printf("%d (%c) Error\n",i,str.charAt(i));
                continue;
            }
            //agregar expancion de transiciones epsilon
            int c = charMap.get(str.charAt(i)); //caracter actual
            ArrayList<Path> next = new ArrayList();
            for (int j = 0; j < activos.size(); j++) {
                //System.out.println(j+","+c);
                Path path = activos.get(j);
                int st = path.state();
                if (deltha[st][0] != null) {
                    for (Integer n : deltha[st][0]) {
                        //System.out.println("Expandiendo " + stateArray[st]);
                        Path npath = (Path) path.clone();
                        npath.add(new Pair(0, n));
                        activos.add(npath);
                    }
                }
                if (deltha[st][c] != null) {
                    for (Integer n : deltha[st][c]) {
                        Path npath = (Path) path.clone();
                        npath.add(new Pair(c, n));
                        next.add(npath);
                    }
                }
            }
            activos = next;
        }
        for (int i = 0; i < activos.size(); i++) {
            Path path = activos.get(i);
            int st = path.state();
            if (deltha[st][0] != null) {
                for (Integer n : deltha[st][0]) {
                    //System.out.println("Expandiendo " + stateArray[st]);
                    Path npath = (Path) path.clone();
                    npath.add(new Pair(0, n));
                    activos.add(npath);
                }
            }
            if (this.isFinal[st]) {
                ret.add(path);
            }
        }
        return ret;
    }

    //construye el recorrido a partir de un objeto de tipo Path y la cadena evaluada
    public String buildPath(Path path, String str) {
        StringBuilder sb = new StringBuilder();
        ArrayList<String> errs = new ArrayList();
        sb.append(stateArray[start]);
        int last = start;
        for (int i = 1, j = 0; j < str.length() || i < path.size(); i++, j++) {
            Pair<Integer, Integer> p = null;
            char c = 0;
            String s = null;
            if (i < path.size()) {
                p = path.get(i);
                c = charArray[p.getFirst()];
                s = stateArray[p.getSecond()];
            }
            if (j < str.length() && !charMap.containsKey(str.charAt(j))) {
                errs.add(stateArray[last] + "-" + str.charAt(j) + "->err");
                if (p != null && p.getFirst() == 0) {
                    sb.append("-").append(c).append("->").append(s);
                    last = p.getSecond();
                    j--;
                } else {
                    i--;
                }
            } else if (p != null) {
                sb.append("-").append(c).append("->").append(s);
                last = p.getSecond();
                if (p.getFirst() == 0) {
                    j--;
                }
            }
        }
        for (String e : errs) {
            sb.append('\n').append(e);
        }
        return sb.toString();
    }

    public HashSet<Integer> epsilonClosure(int state) {
        HashSet<Integer> clo = new HashSet();
        if (deltha[state][0] != null) {
            for (Integer s : deltha[state][0]) {
                clo.add(s);
                clo.addAll(epsilonClosure(s));
            }
        }
        return clo;
    }

    public HashSet<Integer> nextStates(int state, int c) {
        HashSet<Integer> ns = new HashSet();
        if (deltha[state][c] != null) {
            for (Integer s : deltha[state][c]) {
                ns.add(s);
                ns.addAll(epsilonClosure(s));
            }
        }
        return ns;
    }

    public HashSet<Integer> nextStates(HashSet<Integer> states, int c) {
        HashSet<Integer> ns = new HashSet();
        for (Integer s : states) {
            ns.addAll(nextStates(s, c));
        }
        return ns;
    }

    public void toDFA() {
        HashMap<HashSet<Integer>, Integer> newStateMap = new HashMap();
        ArrayList<HashSet<Integer>> newStateArray = new ArrayList();
        HashSet<Integer> first = epsilonClosure(start);
        first.add(start);
        newStateMap.put(first, 0);
        newStateArray.add(first);
        ArrayList<Integer[]> newDeltha = new ArrayList();
        newDeltha.add(new Integer[charArray.length]);
        for (int i = 0; i < newStateArray.size(); i++) {
            for (int j = 1; j < newDeltha.get(i).length; j++) {
                HashSet<Integer> next = nextStates(newStateArray.get(i), j);
                if (!next.isEmpty()) {
                    if (!newStateMap.containsKey(next)) {
                        newStateMap.put(next, newStateArray.size());
                        newDeltha.add(new Integer[charArray.length]);
                        newStateArray.add(next);
                    }
                    newDeltha.get(i)[j] = newStateMap.get(next);
                }
            }
        }
        //System.out.println(newDeltha);
        //imprimir los conjuntos de estados aceptados
        for (HashSet<Integer> set : newStateArray) {
            for (Integer s : set) {
                System.out.print(stateArray[s] + " ");
            }
            System.out.println("");
        }
        //imprimir la nueva tabla de transiciones
        for (Integer[] d : newDeltha) {
            System.out.println(Arrays.toString(d));
        }
        System.out.println(newStateMap.size() + " " + newStateArray.size() + " " + newDeltha.size());

        //guardar la nueva tabla de transiciones y todo lo necesario
        deltha = new ArrayList[newDeltha.size()][charArray.length];
        for (int s = 0; s < newDeltha.size(); s++) {
            for (int c = 1; c < charArray.length; c++) {
                if (newDeltha.get(s)[c] != null) {
                    deltha[s][c] = new ArrayList();
                    deltha[s][c].add(newDeltha.get(s)[c]);
                }
            }
        }
        boolean[] newIsFinal = new boolean[newStateArray.size()];
        stateArray = new String[newStateArray.size()];
        stateMap = new HashMap();
        for (int i = 0; i < newStateArray.size(); i++) {
            newIsFinal[i] = isFinal(newStateArray.get(i));
            stateArray[i] = "q" + i;
            stateMap.put(stateArray[i], i);
        }
        isFinal = newIsFinal;
    }

    private boolean isFinal(HashSet<Integer> states) {
        for (Integer s : states) {
            if (isFinal[s]) {
                return true;
            }
        }
        return false;
    }
}
