/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nfa;

/**
 *
 * @author USUARIO
 */
public class Pair<K, V> {
    private final K first;
    private final V second;

    public Pair(K element0, V element1) {
        this.first = element0;
        this.second = element1;
    }
    
    @Override
    public String toString(){
        return first.toString()+" "+second.toString();
    }

    public K getFirst() {
        return first;
    }

    public V getSecond() {
        return second;
    }
}
